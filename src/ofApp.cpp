#include "ofApp.h"

std::random_device marsenneTwister;
std::mt19937 generator(marsenneTwister());
std::discrete_distribution<> discreteDistribution({1, 85, 7, 7});

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetVerticalSync(false);
    sternPoints.push_back(ofVec2f(0, 0));
    ofBackground(ofColor::black);
}

//--------------------------------------------------------------
void ofApp::update()
{
    for(int i=0; i<100; ++i)
    {
        auto lastPoint = sternPoints.back();

        auto x = 0.0;
        auto y = 0.0;
        auto c = ofColor::white;

        switch (discreteDistribution(generator))
        {
        case 0:
            x = 0;
            y = 0.16 * lastPoint.y;
            c = ofColor::brown;
            break;
        case 1:
            x = 0.85 * lastPoint.x + 0.04 * lastPoint.y;
            y = -0.04 * lastPoint.x + 0.85 * lastPoint.y + 1.6;
            c = ofColor::green;
            break;
        case 2:
            x = 0.2 * lastPoint.x - 0.26 * lastPoint.y;
            y = 0.23 * lastPoint.x + 0.22 * lastPoint.y + 1.6;
            c = ofColor::greenYellow;
            break;
        case 3:
            x = -0.15 * lastPoint.x + 0.28 * lastPoint.y;
            y = 0.26 * lastPoint.x + 0.24 * lastPoint.y + 0.44;
            c = ofColor::darkGreen;
            break;
        }

        sternPoints.push_back(ofVec2f(x, y));
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofSetColor(ofColor::white);
    ofDrawBitmapString("Iterations: "+ofToString(sternPoints.size()), 20, 20);

    ofSetColor(ofColor::greenYellow);
    for (auto p : sternPoints)
    {
        ofDrawCircle(90*p.x + 512, 75*p.y, 0.5);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){}
